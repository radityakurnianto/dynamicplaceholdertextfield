//
//  ViewController.m
//  DynamicPlaceholderTextfieldExample
//
//  Created by yonathan justianus muliawan on 5/16/16.
//  Copyright © 2016 liang. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController{
    NSMutableArray *arrayOfTextfields;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self loadUI];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidLayoutSubviews{
    [super viewDidLayoutSubviews];
    [self reLayout];
}

- (void)loadUI{
    NSArray *placeholder = @[@"Username",
                             @"Password",
                             @"Retype Password",
                             @"Email",
                             @"Address",
                             @"PhoneNumber"];
    arrayOfTextfields = [[NSMutableArray alloc]init];
    for ( int i = 0; i < placeholder.count; i++) {
        YV_DynamicPlaceholderTF *tf = [[YV_DynamicPlaceholderTF alloc]initWithFrame:CGRectMake(20, 20, CGRectGetWidth(self.view.frame)-40, 50)];
        [tf setPlaceholder:placeholder[i]];
        [self.view addSubview:tf];
        [arrayOfTextfields addObject:tf];
        
        if (i == 1 || i == 2) {
            tf.textField.secureTextEntry = YES;
            [tf setSelectedPlaceholderColor:[UIColor redColor]];
            [tf setSelectedPlaceholderFont:[UIFont boldSystemFontOfSize:10]];
        }
        if (i == 3) {
            tf.textField.keyboardType = UIKeyboardTypeEmailAddress;
        }
        if (i == 5) {
            tf.textField.keyboardType = UIKeyboardTypePhonePad;
        }
    }
}

- (void)reLayout{
    CGFloat width = CGRectGetWidth(self.view.frame);
    width -= 40;
    CGRect frame = self.view.bounds;
    frame.origin.y = 20;
    frame.origin.x = 20;
    frame.size.width = width;
    frame.size.height = 50;
    for (YV_DynamicPlaceholderTF *tf in arrayOfTextfields){
        tf.frame = frame;
        frame.origin.y = CGRectGetMaxY(frame)+20;
    }
}

@end
