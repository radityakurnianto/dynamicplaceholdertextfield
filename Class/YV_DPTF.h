//
//  YV_DPTF.h
//  ObjectiveCTestLab
//
//  Created by yonathan justianus muliawan on 5/16/16.
//  Copyright © 2016 liang. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol dynamicPlaceholderDelegate <NSObject>
- (void)DPTFBecomeFirstResponder;
- (void)DPTFResignFirstResponder;
@end

@interface YV_DPTF : UITextField

@property (nonatomic, strong)UIView *viewBottomLine;
@property (nonatomic, strong)UILabel *lblHoveringPlaceholder;

@property (nonatomic, strong)UIColor *unselectedColor;
@property (nonatomic, strong)UIColor *selectedColor;

@property (nonatomic, weak)id <dynamicPlaceholderDelegate> dptfDelegate;

@end
