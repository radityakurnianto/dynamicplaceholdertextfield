//
//  YV_DPTF.m
//  ObjectiveCTestLab
//
//  Created by yonathan justianus muliawan on 5/16/16.
//  Copyright © 2016 liang. All rights reserved.
//

#import "YV_DPTF.h"

@implementation YV_DPTF{
    CGSize size;
}

#pragma mark - initialization
- (instancetype)init{
    self = [super init];
    if (self) {
        [self setProperties];
        [self loadUI];
    }
    return self;
}
- (instancetype)initWithCoder:(NSCoder *)aDecoder{
    self = [super initWithCoder:aDecoder];
    if (self) {
        [self setProperties];
        [self loadUI];
    }
    return self;
}
- (instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
        [self setProperties];
        [self loadUI];
    }
    return self;
}

#pragma mark - override
- (void)setFrame:(CGRect)frame{
    [super setFrame:frame];
    if (!CGSizeEqualToSize(frame.size, size)) {
        size = frame.size;
        [self reLayout];
    }
}
- (BOOL)becomeFirstResponder{
    BOOL returnValue = [super becomeFirstResponder];
    if (returnValue) {
        self.viewBottomLine.backgroundColor = self.selectedColor;
        if (_dptfDelegate) {
            [_dptfDelegate DPTFBecomeFirstResponder];
        }
    }
    return returnValue;
}
- (BOOL)resignFirstResponder{
    BOOL returnValue = [super resignFirstResponder];
    if (returnValue) {
        self.viewBottomLine.backgroundColor = self.unselectedColor;
        if (_dptfDelegate) {
            [_dptfDelegate DPTFResignFirstResponder];
        }
    }
    return returnValue;
}

#pragma mark - starter method
- (void)setProperties{
    self.backgroundColor = [UIColor clearColor];
}
- (void)loadUI{
    [self addSubview:self.viewBottomLine];
    [self addSubview:self.lblHoveringPlaceholder];
}

#pragma mark - UI Handler
- (void)reLayout{
    CGFloat width = CGRectGetWidth(self.frame);
    CGRect frame = self.viewBottomLine.frame; frame.size.width = width;
    self.viewBottomLine.frame = frame;
    self.viewBottomLine.center = CGPointMake(CGRectGetWidth(self.viewBottomLine.frame)/2, CGRectGetHeight(self.frame)-CGRectGetHeight(self.viewBottomLine.frame)/2);
    NSLog(@"y : %f",CGRectGetMinY(self.viewBottomLine.frame));
    NSLog(@"height : %f",CGRectGetHeight(self.frame));
}

#pragma mark - lazy instatiation
- (UIView *)viewBottomLine{
    if (!_viewBottomLine) {
        _viewBottomLine = [[UIView alloc]initWithFrame:CGRectMake(0, 0, 0, 0.5)];
        _viewBottomLine.backgroundColor = self.unselectedColor;
        _viewBottomLine.userInteractionEnabled = NO;
    }
    return _viewBottomLine;
}

- (UILabel *)lblHoveringPlaceholder{
    if (!_lblHoveringPlaceholder) {
        _lblHoveringPlaceholder = [UILabel new];
    }
    return _lblHoveringPlaceholder;
}

- (UIColor *)unselectedColor{
    if (!_unselectedColor) {
        _unselectedColor = [UIColor colorWithRed:224/255.0 green:224/255.0 blue:224/255.0 alpha:1];
    }
    return _unselectedColor;
}
- (UIColor *)selectedColor{
    if (!_selectedColor) {
        _selectedColor = [UIColor colorWithRed:42/255.0 green:185/255.0 blue:110/255.0 alpha:1];
    }
    return _selectedColor;
}

@end
